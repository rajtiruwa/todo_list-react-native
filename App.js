import { StatusBar } from 'expo-status-bar';
import React, {useState} from 'react';
import { Keyboard, KeyboardAvoidingView, Platform, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import Task from "./component/Task"

export default function App() {
  const [task, setTask] = useState();
  const [taskItem, setTaskItem] = useState([]);
  const handleAddTask = () => {
    setTaskItem([...taskItem, task]);
    setTask(null);
    Keyboard.dismiss();
   // console.log(task)
  }

  const handleDeleteTask = (index) => {
    let itemCopy = [...taskItem]
    itemCopy.splice(index, 1)
    setTaskItem(itemCopy)
  } 

  return (
    <View style={styles.container}>
      <View style={styles.textWrapper}>
      <Text style={styles.sectionTitle}>Today's Task</Text>
      <View style={styles.textItems}>
       {
        
        taskItem.map((item, index) => {
          return (
          <TouchableOpacity key={index} onPress={() => handleDeleteTask(index)}>
           <Task text={item}  />
          </TouchableOpacity>
          )
        })
        
       }
      </View>
      </View>
      <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? "padding" : "height"} 
      style={styles.writeTaskWrapper}
      >
        <TextInput style={styles.input} placeholder={'Write a Task'}
        onChangeText={text => setTask(text)}
        value={task}
        />
      
      <TouchableOpacity onPress={() => handleAddTask()} style={styles.addWrapper}>
       <Text style={styles.addText}>+</Text>
      </TouchableOpacity>
      </KeyboardAvoidingView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#E8EAED'
  },

  textWrapper: {
    paddingTop: 80,
    paddingHorizontal: 20
  },

sectionTitle : {
    fontSize: 24,
    fontWeight: 'bold'
},

textItems: {
   marginTop: 30
},

writeTaskWrapper: {
  position: 'absolute',
  bottom: 60,
  width: '100%',
  flexDirection: 'row',
  justifyContent: 'space-around',
  alignItems: 'center'
},

input: {
  paddingHorizontal: 15,
  paddingVertical: 15,
  backgroundColor: "#FFF",
  borderRadius: 60,
  borderColor: '#55BCF6',
  borderWidth: 1,
  width: 250
},
addWrapper: {
  width: 60,
  height: 60,
  borderRadius: 60,
  backgroundColor: '#FFF',
  justifyContent: 'center',
  alignItems: 'center',
  borderColor: '#55BCF6',
  borderWidth: 1,
},
addText: {

},
     
  
});
